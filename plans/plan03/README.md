# Complete build

The goal is now to complete the build and have a real application running (with a simple HTML web site).

Do the following:

* Copy `Dockerfile` and `nginx.conf` to the home directory.
* Copy the modified `.gitlab-ci.yml` file as well.

## Differences

* You have to define a separate runner image for the docker build (cannot be done inside the original build).
* You have to define the Dockerfile, that means the base configuration for the Nginx that is running.