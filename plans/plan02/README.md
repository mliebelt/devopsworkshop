# Introduce the build with errors

The next step is to define the build for creating a docker image.

Resources are:

* Changed build file, integrated docker build (without a build file)

## Result

* The build should fail, because there is no docker build possible without a Dockerfile.