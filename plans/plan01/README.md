# Start with a core build process

What you find in the beginning is the core build, without any (useful) functionality.

What are the parts:

* Defined the structure of development and build.
* Defined the core stages of the build process.
* Filled out the build stage (only) without a visible result.
* Initial content for the web server (but not used).