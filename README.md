# DevOpsWorkshop

This repository is just a starting point to have something to play with. People in the workshop will clone it, and then change at will.

## Play with Docker

Contains some simple examples how to play with Docker. Each one has a separate directory, and a summary file as explanation.

## Play with Kubernetes

Same thing here.

* https://labs.play-with-k8s.com (Login with Docker or Github)

## Plans

1. Start with a core build process, with nearly no functionality.
1. Introduce the build, with errors.
1. Complete the build the first time, with a docker image as result.
1. Change something so that the docker image is changed as well.
1. Deploy the image somewhere.
1. Scale the deployment.

Each of those steps gets its own directory, which is located under plans. You have to copy the files, or create them on your own, if you want.

## Cheat Sheets

To make the exercises on your own, you have to have a basic understanding how Docker and Kubernetes work. Here is a list of cheatsheets for details.

* Docker
  * https://github.com/wsargent/docker-cheat-sheet Very detailed, ...
  * https://devhints.io/docker Very dense, perfect for looking up commands
* Kubernetes
  * https://kubernetes.io/docs/reference/kubectl/cheatsheet/ More a reference (everything you could possibly know ...)
  * https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands Detailed documentation to all commands of `kubectl`.
* Git
  * https://www.git-tower.com/blog/git-cheat-sheet
  
## Base steps in all exercises

* `git clone <URL>` (given in the excercise)
* `cd <DIR>` (depends on the cloned repository)
* `more plan<NR>/README.md` (or look it up in the UI)
* Do some changes in the directory itself, or in `src`.
* `git commit -a -m"<your message>"` (commit all changes)
* `git push` (Push the changes to the central repository)
* Wait for the build server to finish its job. The build server in our exercises is Gitlab-CI, but should work similar with Jenkins, TravisCI, Circle-CI, ...


